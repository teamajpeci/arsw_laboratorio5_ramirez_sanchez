package edu.eci.arsw.thread;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import edu.eci.arsw.GestorUsuarios;
import edu.eci.arsw.messenchat.events.UpdatedUsersListEvent;



public class ThreadListaUsuarios extends Thread {

	ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("system", "manager", "tcp://10.2.67.12:61616");
    MessageConsumer consumer=null;
	Session session;	
	Connection connection;
	Destination destination;
	GestorUsuarios gestor;
	
	
	public void run(){
		
		try{

	connection = connectionFactory.createConnection();
	connection.start();
	 
	session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	destination = session.createTopic("ListaUsuarios");
	consumer = session.createConsumer(destination);	
		

	while (true){
		ObjectMessage mensaje = (ObjectMessage)consumer.receive();
		System.out.println(mensaje);
		UpdatedUsersListEvent listaUsuarios = (UpdatedUsersListEvent)mensaje.getObject();
		
		System.out.println("Llegaron: "+listaUsuarios.getUserNamesList().size()+ " usuarios conectados");
		
		//enviar la lista a todo el mundo

				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);

				}
	     }
		}catch(Exception e){
		System.out.println(e.getMessage() + e.getCause());
		}
	}

		
	public ThreadListaUsuarios(){
		super();

	}

	
	
	
}
