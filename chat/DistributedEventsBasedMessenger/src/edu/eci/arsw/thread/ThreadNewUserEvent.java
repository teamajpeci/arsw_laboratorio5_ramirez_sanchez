package edu.eci.arsw.thread;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

import edu.eci.arsw.GestorUsuarios;
import edu.eci.arsw.messenchat.events.NewUserEvent;
import edu.eci.arsw.messenchat.events.UpdatedUsersListEvent;

public class ThreadNewUserEvent extends Thread{

	ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("system", "manager", "tcp://10.2.67.12:61616");
    MessageConsumer consumer;		//consumidor
    MessageProducer producer;		//productor
	Session session;	
	Connection connection;
	Destination destinologgin;
	Destination destinoListaUsuarios;
	GestorUsuarios gestor;
	
	public void run(){
		
		try{

	connection = connectionFactory.createConnection();
	connection.start();	 
	session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	destinologgin = session.createTopic("LOGGIN");
	consumer = session.createConsumer(destinologgin);	
		
	
	while (true){
		ObjectMessage mensaje = (ObjectMessage)consumer.receive();
		NewUserEvent nuevoUsuario = (NewUserEvent)mensaje.getObject();	
		gestor.getUsersOnline().add(nuevoUsuario.getUserName());
		System.out.println("ahora tengo: "+ gestor.getUsersOnline().size()+"\n");
		publicarListaUsuarios();
		
		
		
		
		
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);

				}
	}
		}catch(Exception e){
		System.out.println(e.getMessage()+"\n");
		}
	}

	
	public void publicarListaUsuarios(){
		
		try {
			
			destinoListaUsuarios = session.createTopic("ListaUsuarios");
			producer = session.createProducer(destinoListaUsuarios);	
			
			UpdatedUsersListEvent lista = new UpdatedUsersListEvent();
			lista.setUserNamesList(gestor.getUsersOnline());
			ObjectMessage message = session.createObjectMessage(lista);
			producer.send(message);
			producer.close();
			
		} catch (JMSException e) {
			e.printStackTrace();
		}
		
		
		
		
	}
	
	
		
	public ThreadNewUserEvent(GestorUsuarios gestor){
		super();
		this.gestor = gestor;
	}
	




}
