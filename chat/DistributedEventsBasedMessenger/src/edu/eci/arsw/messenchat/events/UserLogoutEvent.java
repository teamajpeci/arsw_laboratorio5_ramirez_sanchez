package edu.eci.arsw.messenchat.events;

import java.io.Serializable;

//OJO! NO MODIFIQUE EL NOMBRE NI LOS METODOS DE LA CLASE!

public class UserLogoutEvent implements Serializable {

	private String userName;

	public UserLogoutEvent(String userName) {
		super();
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
