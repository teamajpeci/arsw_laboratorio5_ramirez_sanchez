package edu.eci.arsw.messenchat.events;

import java.io.Serializable;


//OJO! NO MODIFIQUE EL NOMBRE NI LOS METODOS DE LA CLASE!

public class NewUserEvent implements Serializable {

	private String userName;

	public NewUserEvent(String userName) {
		super();
		this.userName = userName;
	}


	public String getUserName() {
		return userName;
	}
	
}
