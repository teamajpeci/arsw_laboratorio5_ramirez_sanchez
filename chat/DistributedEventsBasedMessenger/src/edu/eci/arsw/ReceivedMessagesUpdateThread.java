package edu.eci.arsw;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.swing.JTextArea;

import org.apache.activemq.ActiveMQConnectionFactory;


public class ReceivedMessagesUpdateThread extends Thread {

	private JTextArea area;
	MessageConsumer consumer=null;
	ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("system", "manager", "tcp://10.2.67.12:61616");
	
	Session session;
	Connection connection;
	Destination destination;

	public ReceivedMessagesUpdateThread(JTextArea area) {
		super();
		this.area = area;
	}


	public void run(){
		
			try{
		
		connection = connectionFactory.createConnection();
		connection.start();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		destination = session.createTopic("MENSAJES");
		consumer = session.createConsumer(destination);	
			
		
		while (true){
			ObjectMessage mensaje = (ObjectMessage)consumer.receive();
			area.append("\n"+ " DE: "+((Message)mensaje.getObject()).getFrom()+
					        " PARA:" +((Message)mensaje.getObject()).getTo()+
					           " = "+ ((Message)mensaje.getObject()).getText());
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					}
				
		}
			}catch(Exception e){
				
			}
		
	}
	
}
