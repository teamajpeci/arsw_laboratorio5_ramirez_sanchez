package edu.eci.arsw;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.activemq.ActiveMQConnectionFactory;

import edu.eci.arsw.messenchat.events.NewUserEvent;
import edu.eci.arsw.messenchat.events.UserLogoutEvent;
import edu.eci.arsw.thread.ThreadListaUsuarios;
import edu.eci.arsw.thread.ThreadNewUserEvent;
import edu.eci.arsw.thread.ThreadUserLogoutEvent;



public class MainWindow extends JFrame {

	private JTextArea receivedMessages;	
	private JTextArea messageToSend;
	private JTextArea listaUsuarios;
	private JButton sendButton;
	private JTextField destiny, myname;
	ReceivedMessagesUpdateThread windowUpdateThread;
	ThreadListaUsuarios lista;
	GestorUsuarios gu = new GestorUsuarios();
	static ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("system", "manager", "tcp://10.2.67.12:61616");
	
	
	

	public MainWindow() throws HeadlessException {
		super("MESSENCHAT VERSION 2");
		
		destiny=new JTextField("DESTINY");
		myname=new JTextField("MYNAME");
			
		JPanel addrPanel=new JPanel();
		addrPanel.setLayout(new FlowLayout());
		
		addrPanel.add(destiny);
		addrPanel.add(myname);
		
		receivedMessages=new JTextArea("\n\n\n\n\n\n");
		receivedMessages.setEditable(false);
		listaUsuarios=new JTextArea("Nombre: " + " \n\n\n\n\n\n\n");
		listaUsuarios.setEditable(false);
		messageToSend=new JTextArea();
		sendButton=new JButton("SEND");
		this.setLayout(new BorderLayout());
		JScrollPane topJsp=new JScrollPane();
		topJsp.setSize(topJsp.getWidth(),300);
		JScrollPane botJsp=new JScrollPane();
		JScrollPane westJsp=new JScrollPane();
		westJsp.setSize(300,100);
		
		
		//CREAR Y ASOCIAR EL HILO (SUSCRIPTOR) DE LOS MENSAJES CON LA VENTANA DE TEXTO
		windowUpdateThread=new ReceivedMessagesUpdateThread(receivedMessages);
		windowUpdateThread.start();
		
		topJsp.getViewport().add(receivedMessages);
		botJsp.getViewport().add(messageToSend);
		westJsp.getViewport().add(listaUsuarios);
		this.getContentPane().add(topJsp,BorderLayout.NORTH);
		this.getContentPane().add(botJsp,BorderLayout.CENTER);
		this.getContentPane().add(sendButton,BorderLayout.SOUTH);
		this.getContentPane().add(addrPanel,BorderLayout.EAST);
		this.getContentPane().add(westJsp,BorderLayout.WEST);
		this.setSize(700,700);
		
		sendButton.addActionListener(
				new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent e) {
						String msg=messageToSend.getText();
						Message m=new Message(myname.getText(),destiny.getText(),msg);
						publicar(m);
					}
					
				}
		
		);
		
		
	}
	
	public static void main(String[] args) {
		new MainWindow().setVisible(true);
		conexion();
		
	}
	
	private static void publicar(Message m){
		try{

		Connection connection = connectionFactory.createConnection();
		connection.start();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination destination = session.createTopic("MENSAJES");
		MessageProducer producer = session.createProducer(destination);
		ObjectMessage message = session.createObjectMessage(m); 
		producer.send(message);
		producer.close();
		session.close();
		connection.close();
		}catch(Exception e){
			
		}
		
	} // termina publicar
	
	public static void conexion(){
		try{
		//ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("system", "manager", "tcp://192.168.5.105:61616");
		Connection connection = connectionFactory.createConnection();
		connection.start();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination destination = session.createTopic("LOGGIN");
		MessageProducer producer = session.createProducer(destination);
		NewUserEvent usuarioNuevo = new NewUserEvent("JUAN");
		ObjectMessage message = session.createObjectMessage(usuarioNuevo);  //linea en cuestion
		System.out.println(usuarioNuevo.getUserName());
		producer.send(message);
		producer.close();
		session.close();
		connection.close();
		}catch(Exception e){
			
			System.out.println(e.getMessage() + " ........ " + e.getCause());
		}
	} // termina conexion
	
	public static void desconexion(){
	
		try{
			//ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("system", "manager", "tcp://192.168.5.105:61616");
			Connection connection = connectionFactory.createConnection();
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = session.createTopic("LOGOUT");
			MessageProducer producer = session.createProducer(destination);
			
			UserLogoutEvent usuarioViejo = new UserLogoutEvent("JUAN");	//A falta de creatividad

			ObjectMessage message = session.createObjectMessage(usuarioViejo);  //linea en cuestion
			
			System.out.println(usuarioViejo.getUserName()+"\n");
			producer.send(message);
			
			producer.close();
			session.close();
			connection.close();
			}catch(Exception e){
				
				System.out.println(e.getMessage() + " ........ " + e.getCause());
			}
	
	} // termina desconexion
	
	
}
