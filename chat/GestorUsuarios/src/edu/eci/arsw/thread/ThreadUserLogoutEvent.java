package edu.eci.arsw.thread;

import javax.jms.*;
import org.apache.activemq.ActiveMQConnectionFactory;

import edu.eci.arsw.main.GestorUsuarios;
import edu.eci.arsw.main.UserLogoutEvent;



public class ThreadUserLogoutEvent extends Thread{
	ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("system", "manager", "tcp://192.168.5.105:61616");
    MessageConsumer consumer=null;
	Session session;	
	Connection connection;
	Destination destination;
	GestorUsuarios gestor;
	
	
	public void run(){
		
		try{

	connection = connectionFactory.createConnection();
	connection.start();
	 
	session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	destination = session.createTopic("LOGOUT");
	consumer = session.createConsumer(destination);	
		
	
	while (true){
		ObjectMessage mensaje = (ObjectMessage)consumer.receive();

		UserLogoutEvent viejoUsuario = (UserLogoutEvent)mensaje.getObject();
		System.out.println("Se desconecta: \n"+ viejoUsuario.getUserName()); // para mejorar
		gestor.getUsersOnline().remove(new String(viejoUsuario.getUserName()));
		System.out.println("ahora tengo: "+ gestor.getUsersOnline().size()+"\n");
		//enviar la lista a todo el mundo

				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);

				}
	}
		}catch(Exception e){
		System.out.println(e.getMessage());
		}
	}

		
	public ThreadUserLogoutEvent(GestorUsuarios gestor){
		super();
		this.gestor = gestor;
	}
	


}
