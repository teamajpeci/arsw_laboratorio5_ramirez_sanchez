package edu.eci.arsw.main;


import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import edu.eci.arsw.thread.ThreadListaUsuarios;
import edu.eci.arsw.thread.ThreadNewUserEvent;
import edu.eci.arsw.thread.ThreadUserLogoutEvent;


public class GestorUsuarios {
	
	private List usersOnline = null;
	
	
	public GestorUsuarios(){
		usersOnline = Collections.synchronizedList(new LinkedList());
	
		ThreadNewUserEvent hiloLoggin=new ThreadNewUserEvent(this);
		ThreadUserLogoutEvent hiloLogout = new ThreadUserLogoutEvent(this);
		ThreadListaUsuarios hiloLista = new ThreadListaUsuarios();
		hiloLoggin.start();
		hiloLogout.start();
		hiloLista.start();
		conexion();
		
		
		desconexion();
	}

	
	public List getUsersOnline() {
		return usersOnline;
	}


	public void setUsersOnline(LinkedList<String> usersOnline) {
		this.usersOnline = usersOnline;
	}


	public static void main(String args[]){
		GestorUsuarios gu = new GestorUsuarios();
				
	}
	
	
	
	
	
	public static void conexion(){
		try{
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("system", "manager", "tcp://192.168.5.105:61616");
		Connection connection = connectionFactory.createConnection();
		connection.start();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination destination = session.createTopic("LOGGIN");
		MessageProducer producer = session.createProducer(destination);
		NewUserEvent usuarioNuevo = new NewUserEvent("JUAN");
		ObjectMessage message = session.createObjectMessage(usuarioNuevo);  //linea en cuestion
		System.out.println(usuarioNuevo.getUserName());

		producer.send(message);
		producer.close();
		session.close();
		connection.close();
		}catch(Exception e){
			
			System.out.println(e.getMessage() + " ........ " + e.getCause());
		}
	}
	
	
	
	public static void desconexion(){
	
		try{
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("system", "manager", "tcp://192.168.5.105:61616");
			Connection connection = connectionFactory.createConnection();
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = session.createTopic("LOGOUT");
			MessageProducer producer = session.createProducer(destination);
			
			UserLogoutEvent usuarioViejo = new UserLogoutEvent("JUAN");	//A falta de creatividad

			ObjectMessage message = session.createObjectMessage(usuarioViejo);  //linea en cuestion
			
			System.out.println(usuarioViejo.getUserName()+"\n");
			producer.send(message);
			
			producer.close();
			session.close();
			connection.close();
			}catch(Exception e){
				
				System.out.println(e.getMessage() + " ........ " + e.getCause());
			}
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
